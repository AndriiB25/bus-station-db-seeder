﻿using System;
using System.Collections.Generic;
using BusStation.Models;
using Microsoft.EntityFrameworkCore;

namespace BusStation.Data;

public partial class BusStationContext : DbContext
{
    public BusStationContext()
    {
    }

    public BusStationContext(DbContextOptions<BusStationContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Bus> Buses { get; set; }

    public virtual DbSet<Company> Companies { get; set; }

    public virtual DbSet<CompanyDriver> CompanyDrivers { get; set; }

    public virtual DbSet<Driver> Drivers { get; set; }

    public virtual DbSet<DriversBuse> DriversBuses { get; set; }

    public virtual DbSet<NotBoughtTicket> NotBoughtTickets { get; set; }

    public virtual DbSet<Passenger> Passengers { get; set; }

    public virtual DbSet<PassengersTicket> PassengersTickets { get; set; }

    public virtual DbSet<Route> Routes { get; set; }

    public virtual DbSet<Ticket> Tickets { get; set; }

    public virtual DbSet<Trip> Trips { get; set; }

    public virtual DbSet<TripStatistic> TripStatistics { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSqlServer("Server=DESKTOP-63ST68M;Database=BusStation;Trusted_Connection=SSPI;Encrypt=false;TrustServerCertificate=true");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Bus>(entity =>
        {
            entity.HasKey(e => e.BusId).HasName("PK__Buses__6A0F60B55459850A");

            entity.HasIndex(e => e.BusBrand, "NCLIDX_Buses_Brand");

            entity.HasIndex(e => new { e.BusBrand, e.Capacity }, "NCLIDX_Buses_Covering");

            entity.Property(e => e.BusBrand).HasMaxLength(50);

            entity.HasMany(d => d.Trips).WithMany(p => p.Buses)
                .UsingEntity<Dictionary<string, object>>(
                    "BusesTrip",
                    r => r.HasOne<Trip>().WithMany()
                        .HasForeignKey("TripId")
                        .HasConstraintName("FK__BusesTrip__TripI__0B91BA14"),
                    l => l.HasOne<Bus>().WithMany()
                        .HasForeignKey("BusId")
                        .HasConstraintName("FK__BusesTrip__BusId__0A9D95DB"),
                    j =>
                    {
                        j.HasKey("BusId", "TripId").HasName("PK__BusesTri__9F12A7A6C0C3A826");
                        j.ToTable("BusesTrips");
                        j.HasIndex(new[] { "BusId", "TripId" }, "NCLIDX_BusesTrips_BusTripsIds");
                    });
        });

        modelBuilder.Entity<Company>(entity =>
        {
            entity.HasKey(e => e.CompanyId).HasName("PK__Companie__2D971CAC3236AA6D");

            entity.Property(e => e.CompanyAddress).HasMaxLength(100);
            entity.Property(e => e.CompanyName).HasMaxLength(50);
            entity.Property(e => e.ContactEmail).HasMaxLength(50);
            entity.Property(e => e.ContactPhone).HasMaxLength(30);
        });

        modelBuilder.Entity<CompanyDriver>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("CompanyDrivers");

            entity.Property(e => e.CompanyName).HasMaxLength(50);
            entity.Property(e => e.DriverFullName).HasMaxLength(101);
        });

        modelBuilder.Entity<Driver>(entity =>
        {
            entity.HasKey(e => e.DriverId).HasName("PK__Drivers__F1B1CD04C87FFD3A");

            entity.HasIndex(e => e.BusId, "NCLIDX_Drivers_BusId");

            entity.Property(e => e.DriverLicense).HasMaxLength(30);
            entity.Property(e => e.FirstName).HasMaxLength(50);
            entity.Property(e => e.LastName).HasMaxLength(50);

            entity.HasOne(d => d.Bus).WithMany(p => p.Drivers)
                .HasForeignKey(d => d.BusId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("FK__Drivers__BusId__5165187F");

            entity.HasOne(d => d.Company).WithMany(p => p.Drivers)
                .HasForeignKey(d => d.CompanyId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("FK__Drivers__Company__52593CB8");
        });

        modelBuilder.Entity<DriversBuse>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("DriversBuses");

            entity.Property(e => e.BusBrand).HasMaxLength(50);
        });

        modelBuilder.Entity<NotBoughtTicket>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("NotBoughtTickets");

            entity.Property(e => e.Price).HasColumnType("decimal(10, 2)");
        });

        modelBuilder.Entity<Passenger>(entity =>
        {
            entity.HasKey(e => e.PassengerId).HasName("PK__Passenge__88915FB0CE1F60D5");

            entity.HasIndex(e => e.BusId, "NCLIDX_Passengers_BusId");

            entity.HasIndex(e => e.LastName, "NCLIDX_Passengers_LastName");

            entity.HasIndex(e => e.EmailLocal, "NCLIX_Passenger_EmailLocal");

            entity.Property(e => e.Email).HasMaxLength(50);
            entity.Property(e => e.EmailLocal)
                .HasMaxLength(50)
                .HasComputedColumnSql("(substring([email],(0),charindex('@',[email],(0))))", false);
            entity.Property(e => e.FirstName).HasMaxLength(50);
            entity.Property(e => e.LastName).HasMaxLength(50);
            entity.Property(e => e.PhoneNumber).HasMaxLength(30);

            entity.HasOne(d => d.Bus).WithMany(p => p.Passengers)
                .HasForeignKey(d => d.BusId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("FK__Passenger__BusId__398D8EEE");
        });

        modelBuilder.Entity<PassengersTicket>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("PassengersTickets");

            entity.Property(e => e.FirstName).HasMaxLength(50);
            entity.Property(e => e.Price).HasColumnType("decimal(10, 2)");
        });

        modelBuilder.Entity<Route>(entity =>
        {
            entity.HasKey(e => e.RouteId).HasName("PK__Routes__80979B4DEF647C17");

            entity.HasIndex(e => new { e.Origin, e.Destination }, "NCLIDX_Routes_OrDest");

            entity.Property(e => e.Destination).HasMaxLength(100);
            entity.Property(e => e.Origin).HasMaxLength(100);
        });

        modelBuilder.Entity<Ticket>(entity =>
        {
            entity.HasKey(e => e.TicketId).HasName("PK__Tickets__712CC607176A7ABF");

            entity.ToTable(tb => tb.HasTrigger("UpdateTicketOnPassenger"));

            entity.HasIndex(e => e.PassengerId, "NCLIDX_Tickets_PassengerId");

            entity.HasIndex(e => e.TripId, "NCLIDX_Tickets_TripId");

            entity.Property(e => e.Price).HasColumnType("decimal(10, 2)");

            entity.HasOne(d => d.Passenger).WithMany(p => p.Tickets)
                .HasForeignKey(d => d.PassengerId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK__Tickets__Passeng__123EB7A3");

            entity.HasOne(d => d.Trip).WithMany(p => p.Tickets)
                .HasForeignKey(d => d.TripId)
                .HasConstraintName("FK__Tickets__TripId__0C85DE4D");
        });

        modelBuilder.Entity<Trip>(entity =>
        {
            entity.HasKey(e => e.TripId).HasName("PK__Trips__51DC713EAD7102A3");

            entity.HasIndex(e => e.RouteId, "NCLIDX_Trips_RouteId");

            entity.Property(e => e.ArrivalTime)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.DepartureTime)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.TripStatus).HasMaxLength(30);

            entity.HasOne(d => d.Route).WithMany(p => p.Trips)
                .HasForeignKey(d => d.RouteId)
                .HasConstraintName("FK__Trips__RouteId__4222D4EF");
        });

        modelBuilder.Entity<TripStatistic>(entity =>
        {
            entity.HasKey(e => e.TripStatisticsId).HasName("PK__TripStat__9B3192CCABE03FD8");

            entity.HasIndex(e => e.TripId, "UQ__TripStat__51DC713FCE567086").IsUnique();

            entity.Property(e => e.Revenue).HasColumnType("decimal(10, 2)");

            entity.HasOne(d => d.Trip).WithOne(p => p.TripStatistic)
                .HasForeignKey<TripStatistic>(d => d.TripId)
                .HasConstraintName("FK__TripStati__TripI__07C12930");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
