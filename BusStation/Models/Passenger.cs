﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class Passenger
{
    public int PassengerId { get; set; }

    public string FirstName { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public string? PhoneNumber { get; set; }

    public string? Email { get; set; }

    public int? BusId { get; set; }

    public string? EmailLocal { get; set; }

    public virtual Bus? Bus { get; set; }

    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();
}
