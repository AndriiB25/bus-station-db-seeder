﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class Driver
{
    public int DriverId { get; set; }

    public string FirstName { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public string? DriverLicense { get; set; }

    public int? CompanyId { get; set; }

    public int? BusId { get; set; }

    public virtual Bus? Bus { get; set; }

    public virtual Company? Company { get; set; }
}
