﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class DriversBuse
{
    public string BusBrand { get; set; } = null!;

    public int Capacity { get; set; }
}
