﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class PassengersTicket
{
    public int TripId { get; set; }

    public int SeatId { get; set; }

    public decimal Price { get; set; }

    public string FirstName { get; set; } = null!;
}
