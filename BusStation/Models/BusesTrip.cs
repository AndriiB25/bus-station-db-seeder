﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class BusesTrip
{
    public int BusId { get; set; }

    public int TripId { get; set; }

    public virtual Bus Bus { get; set; } = null!;

    public virtual Trip Trip { get; set; } = null!;
}
