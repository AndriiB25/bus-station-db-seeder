﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class Bus
{
    public int BusId { get; set; }

    public int Capacity { get; set; }

    public string BusBrand { get; set; } = null!;

    public virtual ICollection<Driver> Drivers { get; set; } = new List<Driver>();

    public virtual ICollection<Passenger> Passengers { get; set; } = new List<Passenger>();

    public virtual ICollection<Trip> Trips { get; set; } = new List<Trip>();
}
