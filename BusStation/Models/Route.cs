﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class Route
{
    public int RouteId { get; set; }

    public string Origin { get; set; } = null!;

    public string Destination { get; set; } = null!;

    public virtual ICollection<Trip> Trips { get; set; } = new List<Trip>();
}
