﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class Trip
{
    public int TripId { get; set; }

    public string TripStatus { get; set; } = null!;

    public int RouteId { get; set; }

    public DateTime DepartureTime { get; set; }

    public DateTime ArrivalTime { get; set; }

    public virtual Route Route { get; set; } = null!;

    public virtual ICollection<Ticket> Tickets { get; set; } = new List<Ticket>();

    public virtual TripStatistic? TripStatistic { get; set; }

    public virtual ICollection<Bus> Buses { get; set; } = new List<Bus>();
}
