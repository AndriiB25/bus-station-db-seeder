﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class TripStatistic
{
    public int TripStatisticsId { get; set; }

    public int TicketsSold { get; set; }

    public decimal Revenue { get; set; }

    public int TripId { get; set; }

    public virtual Trip Trip { get; set; } = null!;
}
