﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class Company
{
    public int CompanyId { get; set; }

    public string CompanyName { get; set; } = null!;

    public string CompanyAddress { get; set; } = null!;

    public string? ContactPhone { get; set; }

    public string? ContactEmail { get; set; }

    public virtual ICollection<Driver> Drivers { get; set; } = new List<Driver>();
}
