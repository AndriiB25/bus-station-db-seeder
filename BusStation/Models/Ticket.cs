﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class Ticket
{
    public int TicketId { get; set; }

    public int? PassengerId { get; set; }

    public int TripId { get; set; }

    public decimal Price { get; set; }

    public bool IsSold { get; set; }

    public int? SeatNumber { get; set; }

    public virtual Passenger? Passenger { get; set; }

    public virtual Trip Trip { get; set; } = null!;
}
