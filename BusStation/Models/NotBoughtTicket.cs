﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class NotBoughtTicket
{
    public int TripId { get; set; }

    public int SeatId { get; set; }

    public decimal Price { get; set; }
}
