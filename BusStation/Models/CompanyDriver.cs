﻿using System;
using System.Collections.Generic;

namespace BusStation.Models;

public partial class CompanyDriver
{
    public string DriverFullName { get; set; } = null!;

    public string CompanyName { get; set; } = null!;
}
