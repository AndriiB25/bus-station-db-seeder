﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BusStation.Repositories.Interfaces
{
    public interface IBaseRepository<T> where T : class 
    {
        public IQueryable<T> GetAll();
        public IQueryable<T> GetAll(Expression<Func<T, bool>> conditions);
        public IQueryable<T> GetAllWith(Expression<Func<T, bool>> conditions, params Expression<Func<T, object>>[] includeExpressions);
        public Task<bool> AnyAsync(Expression<Func<T, bool>> conditions);
        public bool Any(Expression<Func<T, bool>> conditions);
        public Task<T?> GetByIdAsync(int id);
        public T? GetById(int id);
        public void Insert(T record);
        public void InsertRange(IEnumerable<T> records);
        public void Update(T record);
        public void Delete(T record);
        public void DeleteIfNotNull(T record);
        public Task<bool> DeleteAsync(int id);
        public void DeleteRange(IEnumerable<T> entities);
        public Task CommitAsync();
        public void Commit();
    }
}
