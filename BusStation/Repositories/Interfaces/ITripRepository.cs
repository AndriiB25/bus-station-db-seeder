﻿using BusStation.Models;

namespace BusStation.Repositories.Interfaces
{
    public interface ITripRepository : IBaseRepository<Trip>
    {
        IEnumerable<Ticket> GetTripTickets(int tripId);

        IEnumerable<(DateTime, DateTime)> GetBusSchedule(int busId);
    }
}
