﻿using BusStation.Models;

namespace BusStation.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        IBusRepository BusRepository { get; }
        ICompanyRepository CompanyRepository { get; }
        IDriverRepository DriverRepository { get; }
        IPassengerRepository PassengerRepository { get; }
        ITripRepository TripRepository { get; }
        IBaseRepository<Route> RouteRepository { get; }

        ITicketRepository TicketRepository { get; }
        IBaseRepository<TripStatistic> TripStatisticRepository { get; }

        void Commit();
        Task CommitAsync();
    }
}
