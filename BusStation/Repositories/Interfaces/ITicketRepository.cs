﻿using BusStation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusStation.Repositories.Interfaces
{
    public interface ITicketRepository : IBaseRepository<Ticket>
    {
        IEnumerable<Ticket> GetAvailableTickets();
    }
}
