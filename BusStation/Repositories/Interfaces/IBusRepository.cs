﻿using BusStation.Models;

namespace BusStation.Repositories.Interfaces
{
    public interface IBusRepository : IBaseRepository<Bus>
    {
        Task<Bus?> GetBusByBrand(string busBrand);

        IEnumerable<Bus> GetBusesWithRoutes();
    }
}
