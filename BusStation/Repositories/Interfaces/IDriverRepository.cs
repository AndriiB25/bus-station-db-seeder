﻿using BusStation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusStation.Repositories.Interfaces
{
    public interface IDriverRepository : IBaseRepository<Driver>
    {
        Task<Driver?> GetDriverByCompany(int companyId);
    }
}
