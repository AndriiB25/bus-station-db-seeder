﻿using BusStation.Models;

namespace BusStation.Repositories.Interfaces
{
    public interface ICompanyRepository : IBaseRepository<Company>
    {
        Task<Company?> GetCompanyByName(string companyName);
        IEnumerable<Company> GetCompaniesWithDrivers();
    }
}
