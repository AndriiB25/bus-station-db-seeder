﻿using BusStation.Data;
using BusStation.Models;
using BusStation.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BusStation.Repositories.Implementations
{
    public class DriverRepository : BaseRepository<Driver>, IDriverRepository
    {
        public DriverRepository(BusStationContext context) : base(context) {}

        public async Task<Driver?> GetDriverByCompany(int companyId)
        {
            return await GetAll(d => d.CompanyId == companyId).FirstOrDefaultAsync();
        }
    }
}
