﻿using BusStation.Data;
using BusStation.Models;
using BusStation.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusStation.Repositories.Implementations
{
    public class TicketRepository : BaseRepository<Ticket>, ITicketRepository
    {
        public TicketRepository(BusStationContext context) : base(context)
        {
        }

        public IEnumerable<Ticket> GetAvailableTickets() => GetAll(t => !t.IsSold).AsNoTracking();
    }
}
