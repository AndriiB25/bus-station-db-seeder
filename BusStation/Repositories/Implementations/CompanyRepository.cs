﻿using BusStation.Data;
using BusStation.Models;
using BusStation.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BusStation.Repositories.Implementations
{
    public class CompanyRepository : BaseRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(BusStationContext context) : base(context) {  }

        public IEnumerable<Company> GetCompaniesWithDrivers() 
            => GetAll().Include(c => c.Drivers).Where(c => c.Drivers.Count > 0).AsNoTracking();

        public async Task<Company?> GetCompanyByName(string companyName)
            => await GetAll(c => c.CompanyName.ToLower() == companyName.ToLower()).FirstOrDefaultAsync();
    }
}
