﻿using BusStation.Data;
using BusStation.Models;
using BusStation.Repositories.Interfaces;

namespace BusStation.Repositories.Implementations
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BusStationContext _context;

        private IBusRepository _busRepository;
        public IBusRepository BusRepository => _busRepository ??= new BusRepository(_context);

        private ICompanyRepository _companyRepository;
        public ICompanyRepository CompanyRepository => _companyRepository ??= new CompanyRepository(_context);

        private IDriverRepository _driverRepository;
        public IDriverRepository DriverRepository => _driverRepository ??= new DriverRepository(_context);

        private IPassengerRepository _passengerRepository;
        public IPassengerRepository PassengerRepository => _passengerRepository ??= new PassengerRepository(_context);

        private ITripRepository _tripRepository;
        public ITripRepository TripRepository => _tripRepository ??= new TripRepository(_context);

        private IBaseRepository<Route> _routeRepository;
        public IBaseRepository<Route> RouteRepository => _routeRepository ??= new BaseRepository<Route>(_context);

        private ITicketRepository _ticketRepository;
        public ITicketRepository TicketRepository => _ticketRepository ??= new TicketRepository(_context);

        private IBaseRepository<TripStatistic> _tripStatisticRepository;
        public IBaseRepository<TripStatistic> TripStatisticRepository 
            => _tripStatisticRepository ??= new BaseRepository<TripStatistic>(_context);

        public void Commit()
        {
            _context.SaveChanges();
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }

        public UnitOfWork(BusStationContext context)
        {
            _context = context;
        }
    }
}
