﻿using BusStation.Data;
using BusStation.Models;
using BusStation.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BusStation.Repositories.Implementations
{
    public class BusRepository : BaseRepository<Bus>, IBusRepository
    {
        public BusRepository(BusStationContext context) : base(context) { }

        public async Task<Bus?> GetBusByBrand(string busBrand) 
            => await GetAll(b => b.BusBrand.ToLower() == busBrand.ToLower()).FirstOrDefaultAsync();

        public IEnumerable<Bus> GetBusesWithRoutes()
        {
            return GetAll().AsNoTracking()
                .Include(b => b.Trips).ThenInclude(t => t.Route)
                .Where(b => b.Trips.Count > 0);
        }
    }
}
