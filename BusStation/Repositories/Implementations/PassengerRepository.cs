﻿using BusStation.Data;
using BusStation.Models;
using BusStation.Repositories.Interfaces;

namespace BusStation.Repositories.Implementations
{
    public class PassengerRepository : BaseRepository<Passenger>, IPassengerRepository
    {
        public PassengerRepository(BusStationContext context) : base(context)
        {
            
        }

        public IEnumerable<Passenger> GetPassengersByBus(int busId) => GetAll(p => p.BusId == busId).ToList();
    }
}
