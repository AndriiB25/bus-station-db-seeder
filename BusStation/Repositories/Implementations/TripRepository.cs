﻿using BusStation.Data;
using BusStation.Models;
using BusStation.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BusStation.Repositories.Implementations
{
    public class TripRepository : BaseRepository<Trip>, ITripRepository
    {
        public TripRepository(BusStationContext context) : base(context)
        {
            
        }

        public IEnumerable<(DateTime, DateTime)> GetBusSchedule(int busId) 
            => GetAll().Include(t => t.Buses)
            .Where(t => t.Buses.Any(b => b.BusId == busId)).ToList()
            .Select(t => (t.DepartureTime, t.ArrivalTime));

        public IEnumerable<Ticket> GetTripTickets(int tripId)
        {
            return GetAllWith(t => t.TripId == tripId, t => t.Tickets).SelectMany(t => t.Tickets).ToList();
        }
    }
}
