﻿using BusStation.Data;
using BusStation.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace BusStation.Repositories.Implementations
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected BusStationContext Db { get; set; }
        protected DbSet<T> MyDbSet { get; set; }

        public BaseRepository(BusStationContext context)
        {
            Db = context;
            MyDbSet = Db.Set<T>();
        }

        public IQueryable<T> GetAll() => MyDbSet;

        public IQueryable<T> GetAll(Expression<Func<T, bool>> conditions)
        {
            return MyDbSet.Where(conditions);
        }

        public IQueryable<T> GetAllWith(Expression<Func<T, bool>> conditions, params Expression<Func<T, object>>[] includeExpressions)
        {
            var set = GetAll();
            if (includeExpressions.Any())
            {
                foreach (var includeExpression in includeExpressions)
                {
                    set = set.Include(includeExpression);
                }
            }

            return set.Where(conditions);
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> conditions) => await MyDbSet.AnyAsync(conditions);

        public bool Any(Expression<Func<T, bool>> conditions) => MyDbSet.Any(conditions);

        public async Task<T?> GetByIdAsync(int id) => await MyDbSet.FindAsync(id);

        public T? GetById(int id) => MyDbSet.Find(id);

        public void Insert(T record)
        {
            var dbe = Db.Entry(record);

            if (dbe.State != EntityState.Detached)
            {
                dbe.State = EntityState.Added;
            }
            else
            {
                MyDbSet.Add(record);
            }
        }

        public void InsertRange(IEnumerable<T> records)
        {
            MyDbSet.AddRange(records);
        }

        public void Update(T record)
        {
            var dbe = Db.Entry(record);

            if (dbe.State == EntityState.Detached)
            {
                MyDbSet.Attach(record);
            }

            Db.Entry(record).State = EntityState.Modified;
        }

        public void DeleteIfNotNull(T record)
        {
            if (record != null)
            {
                Db.Remove(record);
            }
        }

        public void Delete(T record)
        {
            Db.Remove(record);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var record = await GetByIdAsync(id);
            if (record != null)
            {
                Db.Remove(record);
                return true;
            }

            return false;
        }

        public void DeleteRange(IEnumerable<T> entities)
        {
            MyDbSet.RemoveRange(entities);
        }

        public void Commit()
        {
            Db.SaveChanges();
        }

        public async Task CommitAsync()
        {
            await Db.SaveChangesAsync();
        }
    }
}
