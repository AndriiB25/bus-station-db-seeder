﻿using BusStation.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SeederApp;

//var dbOptions = new DbContextOptionsBuilder<BusStationContext>()
//    .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddDebug())).Options;
//using var context = new BusStationContext(dbOptions);
FakeEntitiesGenerator.GenerateBusesTrips();
//context.Database.ExecuteSqlRaw("DBCC CHECKIDENT('Passengers', RESEED, 0);");
//context.Database.ExecuteSqlRaw("DBCC CHECKIDENT('Seats', RESEED, 100);");
//context.Database.ExecuteSqlRaw("DBCC CHECKIDENT('Tickets', RESEED, 0);");
//context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT Passengers ON;");
//context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT Seats ON;");

//context.Buses.AddRange(FakeEntitiesGenerator.GenerateBuses());
//context.Passengers.AddRange(FakeEntitiesGenerator.GeneratePassengers());
//context.Seats.AddRange(FakeEntitiesGenerator.GenerateSeats());
//context.Routes.AddRange(FakeEntitiesGenerator.GenerateRoutes());
//context.Trips.AddRange(FakeEntitiesGenerator.GenerateTrips());
//context.TripTimes.AddRange(FakeEntitiesGenerator.GenerateTripTimes());
//context.TripStatistics.AddRange(FakeEntitiesGenerator.GenerateTripStatistics());
//context.Tickets.AddRange(FakeEntitiesGenerator.GenerateTickets());
//context.Companies.AddRange(FakeEntitiesGenerator.GenerateCompanies());
//context.Drivers.AddRange(FakeEntitiesGenerator.GenerateDrivers());
//FakeEntitiesGenerator.GenerateCompanies();
//context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT Tickets ON;");
//context.SaveChanges();
//Console.WriteLine("Finish");
