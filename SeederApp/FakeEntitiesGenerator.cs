﻿using Bogus;
using Bogus.Bson;
using BusStation.Models;
using Spectre.Console;
using System.Drawing;
using System.Drawing.Printing;

namespace SeederApp
{
    public static class FakeEntitiesGenerator
    {
        private const int PassengersCount = 150;
        private const int BusesCount = 200;
        private const int CompaniesCount = 50;
        private const int DriversCount = 50;
        private const int RoutesCount = 50;
        private const int TripsCount = 50, BusesTripsCount = 150;
        private const int SeatsCount = 150;
        private const int TripTimesCount = 50;
        private const int TicketsCount = 150;
        private const int TripStatisticsCount = 50;

        private const int MinBusCapacity = 15, MaxBusCapacity = 40;

        public static List<Passenger> GeneratePassengers()
        {
            var faker = new Faker<Passenger>()
                   .RuleFor(bp => bp.FirstName, f => f.Name.FirstName())
                   .RuleFor(bp => bp.LastName, f => f.Name.LastName())
                   .RuleFor(bp => bp.PhoneNumber, f => f.Random.Bool(0.2f) ? null : f.Phone.PhoneNumber())
                   .RuleFor(bp => bp.Email, (f, bp) => f.Random.Bool(0.2f) ? null : f.Internet.Email(bp.FirstName, bp.LastName))
                   .RuleFor(bp => bp.BusId, f => f.Random.Bool(0.3f) ? null : f.Random.Int(1, BusesCount));

            var passengers = faker.Generate(PassengersCount).ToList();

            //Print(passengers);

            return passengers;
        }

        public static List<Bus> GenerateBuses()
        {
            var busNames = new string[]
            {
                "Mercedes-Benz",
                "Volkswagen",
                "Opel",
                "Renault",
                "Citroen",
                "Pegeout",
                "Volvo",
                "Neoplan",
                "BMW",
                "Ataman",
                "Ford",
                "MAN",
                "Van Hool",
                "Bogdan"
            };

            var faker = new Faker<Bus>()
                   .RuleFor(b => b.Capacity, f => f.Random.Number(MinBusCapacity, MaxBusCapacity))
                   .RuleFor(b => b.BusBrand, f => f.PickRandom(busNames));

            var buses = faker.Generate(BusesCount).ToList();

            //Print(buses);

            return buses;
        }

        public static List<Seat> GenerateSeats()
        {
            var faker = new Faker<Seat>()
            .RuleFor(s => s.BusId, f => f.Random.Number(1, BusesCount))
            .RuleFor(s => s.SeatNumber, (f, s) => f.Random.Number(1, MaxBusCapacity));

            var seats = faker.Generate(50).ToList();

            //Print(seats);

            return seats;
        }

        public static List<Route> GenerateRoutes()
        {
            var cityNames = new List<string>
            {
                "New York",
                "Los Angeles",
                "Chicago",
                "Houston",
                "Phoenix",
                "Philadelphia",
                "San Antonio",
                "San Diego",
                "Dallas",
                "San Jose",
                "Austin",
                "Jacksonville",
                "San Francisco",
                "Columbus",
                "Indianapolis",
                "Seattle",
                "Denver",
                "Washington",
                "Boston",
                "Nashville",
                "Kyiv",
                "Kharkiv",
                "Odesa",
                "Dnipro",
                "Lviv",
                "Zaporizhzhia",
                "Ivano-Frankivsk",
                "Vinnytsia",
                "Zhytomyr",
                "Lutsk",
                "Poltava",
                "Cherkasy",
                "Chernihiv",
                "Sumy",
                "Rivne",
                "Ternopil",
                "Uzhhorod",
                "Donetsk",
                "Mariupol",
            };

            var faker = new Faker<Route>()
                .RuleFor(r => r.Origin, f => f.PickRandom(cityNames))
                .RuleFor(r => r.Destination, (Faker f, Route r) => f.PickRandom(cityNames.Where((city => city != r.Origin))));

            var routes = faker.Generate(RoutesCount).ToList();

            //Print(routes);

            return routes;
        }

        public static List<Trip> GenerateTrips()
        {
            var faker = new Faker<Trip>()
            .RuleFor(t => t.TripStatus, f => f.PickRandom("Scheduled", "In Progress", "Completed", "Canceled", "On platform", "Left"))
            //.RuleFor(t => t.BusId, f => f.Random.Int(1, BusesCount))
            .RuleFor(t => t.RouteId, f => f.Random.Int(1, RoutesCount));

            var trips = faker.Generate(TripsCount);

            //Print(trips);

            return trips;
        }

        public static List<TripTime> GenerateTripTimes()
        {
            var tripIds = Enumerable.Range(1, TripsCount).ToList();

            var faker = new Faker<TripTime>()
            .RuleFor(tt => tt.TripId, f =>
            {
                var index = f.Random.Int(0, tripIds.Count - 1);
                var tripId = tripIds[index];
                tripIds.RemoveAt(index);
                return tripId;
            })
            .RuleFor(tt => tt.DepartureTime, f => f.Date.Past(1))
            .RuleFor(tt => tt.ArrivalTime, (Faker f, TripTime tt) => f.Date.Between(tt.DepartureTime, DateTime.Now));

            var tripTimes = faker.Generate(TripTimesCount).ToList();

            //Print(tripTimes);

            return tripTimes;
        }

        public static List<TripStatistic> GenerateTripStatistics()
        {
            var tripIds = Enumerable.Range(1, TripsCount).ToList();

            var faker = new Faker<TripStatistic>()
                .RuleFor(ts => ts.TripId, f =>
                {
                    var index = f.Random.Int(0, tripIds.Count - 1);
                    var tripId = tripIds[index];
                    tripIds.RemoveAt(index);
                    return tripId;
                })
                .RuleFor(ts => ts.TicketsSold, f => f.Random.Int(1, 100))
                .RuleFor(ts => ts.Revenue, f => f.Random.Decimal(10, 1000));

            var tripStatistics = faker.Generate(TripStatisticsCount).ToList();

            //Print(tripStatistics);

            return tripStatistics;
        }

        public static List<Ticket> GenerateTickets()
        {
            var seatsIds = Enumerable.Range(1, SeatsCount).ToList();
            var faker = new Faker<Ticket>()
                .RuleFor(t => t.PassengerId, f => f.Random.Bool(0.3f) ? null : f.Random.Int(1, PassengersCount))
                .RuleFor(t => t.TripId, f => f.Random.Int(1, TripsCount))
                .RuleFor(t => t.SeatId, f =>
                {
                    var index = f.Random.Int(0, seatsIds.Count - 1);
                    var seatId = seatsIds[index];
                    seatsIds.RemoveAt(index);
                    return seatId;
                })
                .RuleFor(t => t.Price, f => Math.Round(f.Random.Decimal(50, 1000), 2))
                .RuleFor(t => t.IsSold, (f, t) => t.PassengerId != null);

            var tickets = faker.Generate(TicketsCount).ToList();

            //Print(tickets);

            return tickets;
        }

        public static List<Company> GenerateCompanies()
        {
            var faker = new Faker<Company>()
            .RuleFor(c => c.CompanyName, f => f.Company.CompanyName())
            .RuleFor(c => c.CompanyAddress, f => f.Address.City())
            .RuleFor(c => c.ContactPhone, f => f.Random.Bool(0.3f) ? null : f.Phone.PhoneNumber())
            .RuleFor(c => c.ContactEmail, (f, c) => f.Internet.Email(c.CompanyName));

            var companies = faker.Generate(CompaniesCount).ToList();

            //Print(companies);

            return companies;
        }

        public static List<Driver> GenerateDrivers()
        {
            var faker = new Faker<Driver>()
            .RuleFor(d => d.FirstName, f => f.Name.FirstName())
            .RuleFor(d => d.LastName, f => f.Name.LastName())
            .RuleFor(d => d.DriverLicense, f => f.Random.Bool(0.3f) ? null : f.Random.AlphaNumeric(10))
            .RuleFor(d => d.CompanyId, f => f.Random.Bool(0.3f) ? null : f.Random.Number(1, CompaniesCount))
            .RuleFor(d => d.BusId, f => f.Random.Bool(0.3f) ? null : f.Random.Number(1, BusesCount));

            var drivers = faker.Generate(DriversCount).ToList();

            //Print(drivers);

            return drivers;
        }

        // there is no table in db, only for generating data to insert in table in SSMS redactor
        public static List<BusesTrip> GenerateBusesTrips()
        {
            var busesIds = Enumerable.Range(1, BusesCount).ToList();
            var tripsIds = Enumerable.Range(1, TripsCount).ToList();

            var faker = new Faker<BusesTrip>()
            .RuleFor(bt => bt.BusId, f => f.Random.Int(1, BusesCount))
            .RuleFor(bt => bt.TripId, f => f.Random.Int(1, TripsCount));

            var busesTrips = faker.Generate(BusesTripsCount).ToList();

            // geneius move to copypaste
            foreach (var value in busesTrips)
            {
                Console.WriteLine($"({value.BusId}, {value.TripId}),");
            }

            //Print(busesTrips);

            return busesTrips;
        }

        private static void Print<T> (IEnumerable<T> values)
        {
            foreach(var value in values)
            {
                Console.WriteLine(value?.ToString());
            }
        }
    }
}
